/*********************
*   Robert Koteles
*   Web developer
*   2017
*********************/

/*********************
*  GENERAL FUNCTIONS
*********************/

var GeneralFunctions = {
    prevent_default: function (event) {
        'use strict';
        if (window.event) {
            window.event.returnValue = false;
        } else if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    },
    backToTop: function (event) {
        'use strict';
        $('.back-to-top').on('click', function() {
            var scrollTo = 0;

            $('body,html').animate({
                scrollTop: scrollTo
            }, 500);

            return false;
        });
    }

};

/*********************
*  SPECIFIC FUNCTIONS
*********************/

var Header = {
    initialize: function () {
        var self = this;
        self.onScroll();
    },
    onScroll: function () { /*depends on scroll direction we use different class names*/
        'use strict';
        var self = this;
        var scrollDirection;
        var scrollPositionNew;
        var scrollPositionOld = $(window).scrollTop();
        $(window).on('scroll', function () {
            scrollPositionNew = $(window).scrollTop();
            if (scrollPositionOld < 140) {
                scrollDirection = '';
            } else {
                if (scrollPositionOld > scrollPositionNew) {
                    scrollDirection = 'scrolling-up';
                }
                if (scrollPositionOld < scrollPositionNew) {
                    scrollDirection = 'scrolling-down';
                }
            }
            scrollPositionOld = scrollPositionNew;
            self.handleState(scrollDirection);
        });
    },
    handleState: function (scrollDirection) {
        'use strict';
        $('body').removeClass('scrolling-up scrolling-down').addClass(scrollDirection);
    }
};

var Navigation = {
    initialize: function () {
        var self = this;
        $('#primary-menu li').mouseenter(function () {
            self.onOpen();
        });

        $('#primary-menu li').mouseleave(function () {
            self.onClose();
        });

        self.onClick();
    },
    onClick: function () { /*click events*/
        'use strict';
        var self = this;
        $('img').on('click', function() {
            window.location.href = "index.html";
        });
        $("#sidebar-toggle").on("click", function() {
            GeneralFunctions.prevent_default();
            $("main").toggleClass("sidebar-open");
        });
        $("#mobilemenu-trigger").on("click", function() {
            GeneralFunctions.prevent_default();
            $(this).parents("header").toggleClass("mobilemenu-open");
        });
        
        
    },
    onOpen: function () { /*if we have submenu*/
        'use strict';
        $('body').addClass('navigation-visible');
    },
    onClose: function () {
        'use strict';
        $('body').removeClass('navigation-visible');
    }
};

var Teaser = {
    initialize: function () {
        var self = this;
        var initSetEqualHeight = true;
        self.setEqualHeight('.teaser');

    },
    setEqualHeight: function (_group) { /*set the height of teasers in the same row to equal*/
        var self = this;

        $(_group).addClass('visible');

        if (!self.initSetEqualHeight) {
            $(_group).removeClass('heightSet').css('height', 'auto');
        }

        self.initSetEqualHeight = false;

        if (parseInt($('.measurement').css('max-width'), 10) <= 480) {
            /* mobile */
            return false;
        }

        var _this;
        var _thisOffsetTop = 0;
        var _currentOffsetTop = 0;
        var prevMaxHeight = 0;
        var maxHeight = 0;
        var i = 0;

        $(_group).each(function () {

            _this = $(this);
            _thisOffsetTop = _this.offset().top;

            if (!i) {
                _currentOffsetTop = _thisOffsetTop;
                i += 1;
            }

            prevMaxHeight = maxHeight;
            maxHeight = (_this.outerHeight() > maxHeight) ? _this.outerHeight() : maxHeight;

            if (_currentOffsetTop < (_thisOffsetTop - 10)) {
                _currentOffsetTop = _thisOffsetTop;
                $('.heightSet').css('height', prevMaxHeight + 'px');
                maxHeight = _this.outerHeight();
                $('.heightSet').removeClass('heightSet');
            }
            _this.addClass('heightSet');
        });

        $('.heightSet').css('height', maxHeight + 'px');
        $('.heightSet').removeClass('heightSet');
    }
};


var Footer = {
    initialize: function () {
        var self = this;
    }
};

$(function () {

    Header.initialize();
    Navigation.initialize();
    Teaser.initialize();
    Footer.initialize();

});


$(window).resize(function () {
     Teaser.setEqualHeight('.teaser');
     $("main").addClass("sidebar-open");
});

$(window).on("load", function () {

});







